package com.vano.simpleServer.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.io.IOException;


public class SimpleHandler extends AbstractWebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(SimpleHandler.class);
    private static final String SPLITTER = "----------------------------------------";

    public SimpleHandler() {
        logger.info(SPLITTER + "SimpleHandler is created");
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage json) {
        logger.info(SPLITTER + json.getPayload());
        try {
            //TextMessage message = new TextMessage(json.getPayload().getBytes());
            session.sendMessage(new TextMessage("Ok"));
        } catch (IOException e) {
           // e.printStackTrace();
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        logger.info(SPLITTER + "connection established");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        logger.info(SPLITTER + "Connection closed, status " + status.toString());
    }
}




